package plugins.ylemontag.matlabcommunicator;

import icy.common.Version;
import icy.file.FileUtil;
import icy.gui.frame.progress.ToolTipFrame;
import icy.network.NetworkUtil;
import icy.plugin.PluginDescriptor;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginDaemon;
import icy.preferences.XMLPreferences;
import icy.system.thread.ThreadUtil;
import icy.util.ClassUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Deamon plugin used to initialize the Matlab communicator server
 */
public class MatlabCommunicatorDeamon extends Plugin implements PluginDaemon
{
	private static MatlabCommunicatorServer       _instance     = null;
	private static Map<String, MatlabSession>     _sessions     = new HashMap<String, MatlabSession>();
	private static Map<String, MatlabInterpreter> _interpreters = new HashMap<String, MatlabInterpreter>();
	private static final String PLUGIN_DIRECTORY   = "plugins/ylemontag/matlabcommunicator";
	private static final String SOURCE_FOLDER_FILE = PLUGIN_DIRECTORY + "/source_folders.txt";
	
	private void showInstallMessage()
	{
		String message = //TODO: remove the intrusive <br/> in the tooltip 
			"<div style=\"font-weight: bold; font-size: 105%; margin-bottom: 8px;\">" +
				"You've just install the Matlab communicator plugin" +
			"</div>" +
			"<div style=\"margin-bottom: 8px;\">" +
				"To use this plugin, you have to <strong>configure your Matlab application</strong> in a specific way.<br/>" + 
				"In particular, the following commands must be executed each time you start Matlab:<br/>" +
				"<tt>&gt;&gt; addpath('/path/to/Icy/plugins/ylemontag/matlabcommunicator');</tt><br/>" +
				"<tt>&gt;&gt; icy_init();</tt><br/>" +
			"</div>" +
			"<div style=\"margin-bottom: 8px;\">" +
				"This operation can be done automatically using a <tt>startup.m</tt> file " +
				"(please consult<br/>the Matlab documentation for more details)." +
			"</div>" +
			"<div style=\"margin-bottom: 8px;\">" +
				"These instructions are also mentioned in the <tt>icy_init.m</tt> file, " +
				"located in the<br/><tt>/path/to/icy/plugins/ylemontag/matlabcommunicator</tt> directory." +
			"</div>"
		;
		new ToolTipFrame(message, "plugins.ylemontag.matlabcommunicator.MatlabCommunicatorDeamon.InstallMessage");
	}
	
	/**
	 * Retrieve the previously registered version number for a MatlabInterpreter plugin
	 */
	private Version getMatlabFunctionVersion(String className)
	{
		try {
			XMLPreferences node = getPreferences("InterpreterVersions");
			return new Version(node.get(className, ""));
		}
		catch(NumberFormatException err) {}
		return new Version();
	}
	
	/**
	 * Set the version number of a MatlabInterpreter plugin
	 */
	private void setMatlabFunctionVersion(String className, Version version)
	{
		XMLPreferences node = getPreferences("InterpreterVersions");
		node.put(className, version.toString());
	}
	
	/**
	 * Extract a .m file from the jar archive
	 */
	private void extractMatlabFunction(PluginDescriptor descriptor, String functionName)
		throws IOException
	{
		String      fileName = ClassUtil.getPathFromQualifiedName(descriptor.getPackageName()) + "/" + functionName + ".m";
		InputStream resource = getResourceAsStream(fileName);
		if(resource==null) {
			throw new IOException("Cannot found the resource " + fileName);
		}
		byte[] buffer = NetworkUtil.download(resource);
		FileUtil.ensureParentDirExist(fileName);
		OutputStream file = new FileOutputStream(fileName);
		file.write(buffer);
		file.close();
	}
	
	/**
	 * List all the Matlab function files contained the plugin folder corresponding
	 * to the given plugin descriptor
	 */
	private Set<String> getExistingMatlabFunctions(PluginDescriptor descriptor)
		throws IOException
	{
		String            folder = ClassUtil.getPathFromQualifiedName(descriptor.getPackageName());
		ArrayList<String> files  = FileUtil.getFileListAsString(folder, false, false, false);
		Set<String>       retVal = new HashSet<String>();
		for(String f : files) {
			String extension = FileUtil.getFileExtension(f, false);
			if(extension.equals("m")) {
				retVal.add(FileUtil.getFileName(f, false));
			}
		}
		return retVal;
	}
	
	/**
	 * Change the extension of a .m file into .m.old
	 */
	private void deprecateMatlabFunction(PluginDescriptor descriptor, String functionName)
		throws IOException
	{
		String fileName = ClassUtil.getPathFromQualifiedName(descriptor.getPackageName()) + "/" + functionName + ".m";
		if(FileUtil.exists(fileName)) {
			FileUtil.rename(fileName, fileName + ".old", false);
		}
	}
	
	/**
	 * Clear the file containing the list of source folders
	 */
	private void clearSourceFolders()
	{
		FileUtil.delete(SOURCE_FOLDER_FILE, false);
	}
	
	/**
	 * Register the source folder corresponding to a MatlabInterpreter plugin in
	 * the dedicated file
	 */
	private void appendSourceFolder(PluginDescriptor descriptor) throws IOException
	{
		File   folder = new File(ClassUtil.getPathFromQualifiedName(descriptor.getPackageName()));
		String entry  = folder.getAbsolutePath() + "\n";
		byte[] buffer = entry.getBytes("UTF-8");
		FileUtil.ensureParentDirExist(SOURCE_FOLDER_FILE);
		OutputStream file = new FileOutputStream(SOURCE_FOLDER_FILE, true);
		file.write(buffer);
		file.close();
	}
	
	/**
	 * Clear the list of interpreters
	 */
	private void clearInterpreterInstances()
	{
		synchronized (_interpreters) {
			_interpreters.clear();
		}
	}
	
	/**
	 * Register a plugin instance in the MatlabInterpreter index
	 */
	private void appendInterpreterInstance(String className, MatlabInterpreter interpreter)
	{
		synchronized (_interpreters) {
			_interpreters.put(className, interpreter);
		}
	}
	
	/**
	 * Extract the .m files from the .jar plugin archive and write them as individual files
	 * in the plugin directory
	 */
	private void installMatlabFunctions(PluginDescriptor descriptor, String ... functions) throws IOException
	{
		// Current plugin version
		Version currentVersion = descriptor.getVersion();
		if(currentVersion.isEmpty()) {
			return; // Empty version correspond to the development version
		}
		
		// Known version for the plugin
		Version knownVersion = getMatlabFunctionVersion(descriptor.getClassName());
		if(!knownVersion.isEmpty() && currentVersion.equals(knownVersion)) {
			return;
		}
		
		// Install the Matlab function files, remove the old one, and update the known version
		Set<String> existingFunctions = getExistingMatlabFunctions(descriptor);
		for(String fun : functions) {
			existingFunctions.remove(fun);
			extractMatlabFunction(descriptor, fun);
		}
		for(String fun : existingFunctions) {
			deprecateMatlabFunction(descriptor, fun);
		}
		setMatlabFunctionVersion(descriptor.getClassName(), currentVersion);
	}
	
	/**
	 * Register a MatlabInterpreter plugin
	 */
	private void registerInterpreter(PluginDescriptor descriptor)
		throws IOException, InstantiationException, IllegalAccessException
	{
		// Append the plugin folder to the list of folders that should be append to
		// the Matlab path
		appendSourceFolder(descriptor);
		
		// Save the instance of the plugin in the MatlabInterpreter index
		MatlabInterpreter instance = (MatlabInterpreter)descriptor.getPluginClass().newInstance();
		appendInterpreterInstance(descriptor.getClassName(), instance);
		
		// Install the .m files and update the file read by the icy_init function
		installMatlabFunctions(descriptor, instance.getFunctionNames());
	}
	
	@Override
	public void init()
	{
		// Create the server
		if(_instance!=null) {
			throw new IllegalStateException(
				"Trying to create a new instance of the Matlab communicator server while one is already running"
			);
		}
		_instance = new MatlabCommunicatorServer(_sessions, _interpreters);
		
		// Extract the Matlab function files
		try {
			installMatlabFunctions(getDescriptor(), "icy_init", "icy_command");
		}
		catch(IOException err) {
			reportException(err);
		}
		
		// Register all the interpreters
		clearInterpreterInstances();
		clearSourceFolders();
		for(PluginDescriptor descriptor : PluginLoader.getPlugins(MatlabInterpreter.class)) {
			try {
				registerInterpreter(descriptor);
			}
			catch(IOException err) {
				reportException(err);
			}
			catch(RuntimeException err) {
				throw err;
			}
			catch(Exception err) {
				throw new RuntimeException(err);
			}
		}
	}
	
	@Override
	public void run()
	{
		ThreadUtil.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				showInstallMessage();
			}
		});
		
		MatlabCommunicatorServer currentInstance = _instance;
		if(currentInstance!=null) {
			try {
				currentInstance.run();
			}
			catch(IOException err) {
				reportException(err);
			}
		}
	}

	@Override
	public void stop()
	{
		if(_instance==null) {
			throw new IllegalStateException(
				"Trying to stop the Matlab communicator server while no instance is currently running"
			);
		}
		try {
			_instance.kill();
			_instance = null;
		}
		catch(IOException err) {
			reportException(err);
		}
	}
	
	/**
	 * Display an error message
	 */
	private static void reportException(final IOException err)
	{
		ThreadUtil.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				System.err.println("Error encountered by the MatlabCommunicatorDeamon plugin:");
				err.printStackTrace();
			}
		});
	}
}
