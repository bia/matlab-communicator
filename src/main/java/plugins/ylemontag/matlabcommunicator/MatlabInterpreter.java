package plugins.ylemontag.matlabcommunicator;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Interface to implement to execute Matlab commands
 */
public interface MatlabInterpreter
{
	/**
	 * Return the names of the entry points to register, i.e. the names of all the
	 * Matlab functions that are provided by the plugin
	 */
	public String[] getFunctionNames();
	
	/**
	 * Execute a command
	 */
	public void execute(String command, MatlabSession session) throws MatlabCommandException;
}
