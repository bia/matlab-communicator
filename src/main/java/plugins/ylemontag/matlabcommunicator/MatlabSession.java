package plugins.ylemontag.matlabcommunicator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Store the variables associated to a Matlab session (i.e. one Matlab client)
 */
public class MatlabSession
{
	private String              _clientId;
	private File                _in      ;
	private File                _out     ;
	private Map<String, Object> _objects ;
	
	/**
	 * Constructor
	 */
	public MatlabSession() throws IOException
	{
		_clientId = allocateClientId();
		_in       = allocateTemporaryFile();
		_out      = allocateTemporaryFile();
		_objects  = new HashMap<String, Object>();
	}
	
	/**
	 * Current client ID
	 */
	public String getClientId()
	{
		return _clientId;
	}
	
	/**
	 * Current input file (communication from Matlab to the Icy server)
	 */
	public File getInput()
	{
		return _in;
	}
	
	/**
	 * Current output file (communication from the Icy server to Matlab)
	 */
	public File getOutput()
	{
		return _out;
	}
	
	/**
	 * Return the object with the given ID
	 */
	public Object get(String name)
	{
		synchronized (_objects) {
			return _objects.get(name);
		}
	}
	
	/**
	 * Set the object with the given ID
	 */
	public Object put(String name, Object obj)
	{
		synchronized (_objects) {
			return _objects.put(name, obj);
		}
	}
	
	/**
	 * Allocate a new client ID
	 */
	private static String allocateClientId()
	{
		return UUID.randomUUID().toString();
	}
	
	/**
	 * Allocate a new temporary Matlab .mat file
	 */
	private static File allocateTemporaryFile() throws IOException
	{
		File temporaryFile = File.createTempFile("icy_", ".mat");
		temporaryFile.deleteOnExit();
		return temporaryFile;
	}
}
