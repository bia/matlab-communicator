package plugins.ylemontag.matlabcommunicator;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Exception thrown by Matlab interpreters plugins when the execution of a command fails
 */
public class MatlabCommandException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public MatlabCommandException()
	{
		super();
	}
	
	public MatlabCommandException(String message)
	{
		super(message);
	}
	
	public MatlabCommandException(Throwable throwable)
	{
		super(throwable);
	}
}
