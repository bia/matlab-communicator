package plugins.ylemontag.matlabcommunicator;

import icy.system.thread.ThreadUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Map;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Set up a server waiting for commands issued by Matlab
 */
public class MatlabCommunicatorServer
{
	private Map<String, MatlabSession>     _sessions;
	private Map<String, MatlabInterpreter> _interpreters;
	private int                            _port;
	private boolean                        _killed;
	private Object                         _runKillMutex;
	private LinkedList<MatlabSocket>       _sockets;
	private ServerSocket                   _socketProvider;
	
	/**
	 * Default constructor (use port 8732)
	 */
	public MatlabCommunicatorServer(Map<String, MatlabSession> sessions,
		Map<String, MatlabInterpreter> interpreters)
	{
		this(sessions, interpreters, 8732);
	}
	
	/**
	 * Constructor
	 */
	public MatlabCommunicatorServer(Map<String, MatlabSession> sessions,
		Map<String, MatlabInterpreter> interpreters, int port)
	{
		_sessions     = sessions;
		_interpreters = interpreters;
		_port         = port;
		_killed       = false;
		_runKillMutex = new Object();
		_sockets      = new LinkedList<MatlabSocket>();
	}
	
	/**
	 * Enter the loop
	 */
	public void run() throws IOException
	{
		// Create the socket if 'kill' has not been already called
		synchronized (_runKillMutex)
		{
			if(_killed) {
				return;
			}
			_socketProvider = new ServerSocket(_port);
		}
		
		// Wait for clients to connect
		while(!_socketProvider.isClosed()) {
			waitForConnection();
		}
	}
	
	/**
	 * Stop the loop
	 */
	public void kill() throws IOException
	{
		synchronized (_runKillMutex)
		{
			_killed = true;
			if(_socketProvider==null || _socketProvider.isClosed()) {
				return;
			}
			synchronized (_sockets) {
				for(MatlabSocket socket : _sockets) {
					socket.kill();
				}
				_sockets.clear();
			}
			_socketProvider.close();
		}
	}
	
	/**
	 * Wait for a client to connect
	 */
	private void waitForConnection() throws IOException
	{
		try {
			final Socket socket = _socketProvider.accept();
			SocketAddress remoteAddress = socket.getRemoteSocketAddress();
			InetSocketAddress inetRemoteAddress = (InetSocketAddress)remoteAddress;
			if(inetRemoteAddress==null || !inetRemoteAddress.getAddress().isLoopbackAddress()) {
				throw new IOException("Unauthorized connection attempt from " + remoteAddress);
			}
			ThreadUtil.bgRun(new Runnable()
			{
				@Override
				public void run() {
					try {
						MatlabSocket connection = new MatlabSocket(socket);
						synchronized (_sockets) {
							_sockets.push(connection);
						}
						connection.run();
					}
					catch(IOException err) {
						reportException(err);
					}
				}
			});
		}
		catch(SocketException se) {} // Exception received when the socket is closed
	}
	
	/**
	 * Encapsulate a socket
	 */
	private class MatlabSocket
	{
		private Socket       _socket;
		private InputStream  _in    ;
		private OutputStream _out   ;
		
		/**
		 * Constructor
		 */
		public MatlabSocket(Socket socket) throws IOException
		{
			_socket = socket;
			_in     = _socket.getInputStream ();
			_out    = _socket.getOutputStream();
		}
		
		/**
		 * Receive the command, send the answer and close the connection
		 */
		public void run() throws IOException
		{
			try {
				String clientId = readLine();
				
				// If the Matlab client haven't got an ID yet, create a new session,
				// and return its parameters to the client
				if(clientId.equals("?")) {
					MatlabSession session = new MatlabSession();
					putSession(session);
					writeLine("ID");
					writeLine(session.getClientId());
					writeLine(session.getInput ().getAbsolutePath());
					writeLine(session.getOutput().getAbsolutePath());
				}
				
				// Otherwise, try to retrieve the session object associated to the client
				else {
					MatlabSession session = getSession(clientId);
					if(session==null) { // If the ID does not exist, return a 'Bad ID' error message
						writeLine("Bad ID");
					}
					
					// Otherwise, try to retrieve the interpreter that is supposed to execute the command
					else {
						String            className   = readLine();
						MatlabInterpreter interpreter = getInterpreter(className);
						if(interpreter==null) // If the class name does not refer to a known Matlab interpreter object,
						{                     // throw a 'Bad class name' error message
							writeLine("Bad class name"); 
						}
						
						// Otherwise, execute the command
						else {
							String command = readLine();
							try {
								interpreter.execute(command, session);
								writeLine("OK");
							}
							catch(MatlabCommandException err) {
								writeLine("Fail");
								writeLine(err.getMessage());
								writeLine("Stack trace:");
								for(StackTraceElement e : err.getStackTrace()) {
									writeLine("  " + e.toString());
								}
							}
						}
					}
				}
			}
			
			// Always close the socket at the end
			finally {
				writeLine("EOF");
				_socket.close();
			}
		}
		
		/**
		 * Stop the loop
		 */
		public void kill() throws IOException
		{
			if(_socket.isClosed()) {
				return;
			}
			_socket.close();
		}
		
		/**
		 * Read the given message (i.e. a text line) in the input stream
		 */
		private String readLine() throws IOException
		{
			String retVal = "";
			while(true) {
				int rawChar = _in.read();
				if(rawChar<0) {
					throw new IOException("End of stream reached unexpectedly");
				}
				else if(rawChar==10 || rawChar==13) {
					return retVal;
				}
				else {
					retVal += (char)rawChar;
				}
			}
		}
		
		/**
		 * Send a message (i.e. a text line) through the output stream
		 */
		private void writeLine(String message) throws IOException
		{
			int len = message.length();
			byte[] buffer = new byte[len+1];
			for(int k=0; k<len; ++k) {
				buffer[k] = (byte)message.charAt(k);
			}
			buffer[len] = 10;
			_out.write(buffer);
			_out.flush();
		}
		
		/**
		 * Return the session object associated to the given client ID, or null if
		 * this client ID is unknown
		 */
		private MatlabSession getSession(String clientId)
		{
			synchronized (_sessions) {
				return _sessions.get(clientId);
			}
		}
		
		/**
		 * Register the given session object
		 */
		private void putSession(MatlabSession session)
		{
			synchronized (_sessions) {
				_sessions.put(session.getClientId(), session);
			}
		}
		
		/**
		 * Return the plugin instance corresponding to the given class name, or
		 * null if this class name has not been registered
		 */
		private MatlabInterpreter getInterpreter(String className)
		{
			synchronized (_interpreters) {
				return _interpreters.get(className);
			}
		}
	}
	
	/**
	 * Display an error message
	 */
	private static void reportException(final IOException err)
	{
		ThreadUtil.invokeLater(new Runnable()
		{
			@Override
			public void run() {
				System.err.println("Error encountered while processing a Matlab command:");
				err.printStackTrace();
			}
		});
	}
}
